export var game_setup = {
    "min_distance_away_from_victim_to_save": 12,

    "starting_locations": [
        [495, 200],
        [505, 200],
        [510, 200],
        [510, 195],
        [505, 195],
    ],
    'scale': 0.15

};

export var user_config = {

    semantic_map_file: "./assets/semantic_maps/Falcon_v1.0_Easy_sm.json",

    "green_victim_point": 10,
    "yellow_victim_point": 10, 
    "green_victim_count" : 15,
    "yellow_victim_count" : 15,
    "game_time_seconds": 360,
    "speed" : 35,
}

