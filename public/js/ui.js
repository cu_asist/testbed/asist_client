function set_display(element, display) {
    Document.getElementById(element).style.display = display;
}

function update_request_button_timer() {
    document.getElementById("askUpdate").innerHTML = "Request Update";
}

function player_request_update_clicked(socket, player_id, room_id) {
    console.log("user clicked ask for update");
    // socket
    document.getElementById("askUpdate").innerHTML = "Request Sent!!";
    setInterval(update_request_button_timer, 2000);
    socket.emit("teammate_update_request", {
        topic: "comm",
        key: "ud_request",
        publisher: "client",
        idx: player_id,
        rm_id: room_id,
    });
    console.log("END: user clicked ask for update");
}


function teammate_request_submit_clicked(socket, player_id) {
    console.log("user clicked teammate request submit");
    var player_medkit = document.getElementById("player_medkit").value;
    var player_green_vic = document.getElementById("player_green_vic").value;
    var player_yellow_vic = document.getElementById("player_yellow_vic").value;
    var player_score = document.getElementById("player_score").value;

    // get the ground truth value
    var player_medkit_groundtruth = document.getElementById(
        "player-med-kits-used"
    ).value;
    var player_green_vic_groundtruth =
        document.getElementById("green_victim_saved").value;
    var player_yellow_vic_groundtruth = document.getElementById(
        "yellow_victim_saved"
    ).value;
    var player_score_groundtruth = document.getElementById("score").value;

    // get asist timer value
    var asist_timer = document.getElementById("timer-text").innerHTML;
    socket.emit("teammate_request_submission", {
        topic: "comm",
        key: "ud_submit",
        publisher: "client",
        idx: player_id,
        asist_timer: asist_timer,
        request_input_form: {
            player_medkit: player_medkit,
            player_green_vic: player_green_vic,
            player_yellow_vic: player_yellow_vic,
            player_score: player_score,
        },
        groundtruth_for_request_input: {
            player_medkit: player_medkit_groundtruth,
            player_green_vic: player_green_vic_groundtruth,
            player_yellow_vic: player_yellow_vic_groundtruth,
            player_score: player_score_groundtruth,
        },
    });
    document.getElementById("request-label").classList.toggle("blink-bg");
    set_display("submit-button-div", "none");
    set_display("cancel-button-div", "none");
}

function teammate_request_cancel_clicked() {
    document.getElementById("request-label").classList.toggle("blink-bg");
    set_display("submit-button-div", "none");
    set_display("cancel-button-div", "none");
}
export {
    set_display,
    player_request_update_clicked,
    teammate_request_submit_clicked,
    teammate_request_cancel_clicked,
};

export function customize_player_ui(player_id) {
    const mapping = {
        texts: {
            "player_type": ['green', 'yellow'],
            "teammate_type": ['yellow', 'green'],
            "player_med_for_green": ["2", "1"], 
            "teammate_med_for_green": ["1", "2"],
            "player_med_for_yellow": ["1", "2"],
            "teammate_med_for_yellow": ["2", "1"],
            "player_rew_for_green": ["0.3", "0.1"], 
            "teammate_rew_for_green": ["0", "0.1"],
            "player_rew_for_yellow": ["0.1", "0.3"],
            "teammate_rew_for_yellow": ["0.1", "0"],
            "player_rew_for_tmgreen": ["0.1","0"], 
            "teammate_rew_for_tmgreen": ["0.1","0.3"],
            "player_rew_for_tmyellow": ["0","0.1"],
            "teammate_rew_for_tmyellow": ["0.3","0.1"],
            // "team_reward": ["3", "5"],
            // "ind_reward": ["5", "3"],
        },
        images: {
            "avatar_image": ["player_g.png", "player_y.png"],
            "avatar_image2": ["player_g.png", "player_y.png"],
            "teammate_avatar_image": ["player_y.png", "player_g.png"],
            "teammate_avatar_image2": ["player_y.png", "player_g.png"],
            "example_game_screen": ["example_green.png", "example_yellow.png"],
            "good_luck_player": ["green_player.png", "yellow_player.png"],
            "teammate-display-img" : ["player_y.png", "player_g.png"],
        }
    };

    for (let [elmt, content] of Object.entries(mapping.texts)) {
        document.getElementById(elmt).textContent = content[player_id];
    }
    for (let [elmt, content] of Object.entries(mapping.images)) {
        document.getElementById(elmt).src = "assets/images/" + content[player_id];
    }

}