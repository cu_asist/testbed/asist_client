import { display_pre_survey, display_post_survey } from "./survey.js"
import {
    customize_player_ui,
} from "./ui.js"

import { user_config, game_setup } from "./config.js"

import Map from "./game/map.js"
import Client from "./client.js"
import GameTimer from "./game/timer.js"



// will be set when game starts socket event 
let game_time_seconds = user_config.game_time_seconds;


// On page load, make the consent-form block visible
document.onreadystatechange = () => {
    document.getElementById("consent-form").style.display = 'block';
}

// Semantic Map
var map = new Map(user_config.semantic_map_file);

// Main testbed object
var testbed = {

    tile_size: 10,
    map: map,
    user_config: user_config,
    game_setup: game_setup,
    asist_timer: new GameTimer(game_time_seconds, "timer-text"),


    wait_data_callback: function(player_id, player_limit, room_id, message){
        console.log("Callback: " + player_id);
        this.player_id = player_id;
        this.player_limit = player_limit;
        this.room_id = room_id;

        if (message["experiment"][0] == 0) {
            switch_block("exp-3-top", "exp-1-2-top","grid");   
            switch_block("automatic-teammate-progress", "request-respond");
            switch_block("teammate-message", "request-respond");
        } else if (message["experiment"][0] == 1){
            switch_block("exp-3-top", "exp-1-2-top","grid");     
            switch_block("request-respond", "automatic-teammate-progress");  
            switch_block("teammate-message", "automatic-teammate-progress");          
        } else if (message["experiment"][0] == 2){
            switch_block("exp-1-2-top", "exp-3-top","grid");   
            switch_block("request-respond", "teammate-message");  
            switch_block("automatic-teammate-progress", "teammate-message");      
        }
    },
    
    intro_ready_callback: function() {
        customize_player_ui(this.player_id);
        display_pre_survey(this.player_id, this.pre_survey_callback);
        switch_block("waiting-for-participants", "instructions");
    },

    pre_survey_callback: function(survey_data) {
        console.log("pre_survey_callback");
        client.survey_completed("pre_survey", survey_data);
        switch_block("pre-survey", "start-game");
    },

    start_game_callback: function() {
        switch_block("waiting-for-teammate", "game-window","grid");
        switch_block("waiting-for-participants", "game-window","grid");
        console.log("Adding scene and starting gameplay");

        // this.start_testbed();
    },
    end_game_callback: function(){
        switch_block("game-window", "post-survey");
        display_post_survey(testbed.post_survey_callback);
    },
    post_survey_callback: function(survey_data) {
        client.survey_completed("post_survey", survey_data);
    },
}


testbed.phaser_config = {
    type: Phaser.AUTO,
    parent: 'game',
    width: testbed.tile_size * map.width,
    heigth: testbed.tile_size * map.height,
    scale: {
        mode: Phaser.Scale.RESIZE,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: { y: 0 },
        },
    },
    transparent: 'true',
};




// Websocket Client
var client = new Client(testbed);

// These functions manage the experiment flow, switching visible
// UI blocks (pages) and communicating with the API server

function switch_block(from, to, switch_type="block") {
    document.getElementById(from).style.display = "none";
    document.getElementById(to).style.display = switch_type;
}

// Button event callback functions to switch blocks
window.agreed_to_consent = function() {
    switch_block("consent-form", "waiting-for-participants");
    client.start_wait();
}

window.start_game_clicked = function() {
    switch_block("start-game", "waiting-for-teammate");
    client.participant_ready();
}

window.take_quiz = function() {
    switch_block("instructions", "pre-survey");
}

window.show_instructions = function() {
    switch_block("pre-survey", "instructions");
}

window.teammate_msg_send = function(){
    console.log(testbed.player_id);
    client.msg_send();
}
