import { socketURL } from "/js/socketaddr.js"

import ASISTScene from "./game/scene.js";

export default class Client {

    constructor(testbed) {

        this.testbed = testbed;
        this.player_id = null;
        this.room_id = null;
        this.prolific = null;

        this.socket = io(socketURL, { transports: ['websocket'] });

        // Callbacks in order of experiment flow

        // Welcome message on connect
        this.socket.on('welcome', (message) => {
            console.log(message["data"]);
            console.log("Web socket connected")
        });
        // player consented and waiting
        this.socket.on('wait_data', (message) => {
            this.room_id      = message["rm_id"]
            this.player_id    = message["idx"]
            this.player_limit = message["player_limit"]
            this.testbed.wait_data_callback(this.player_id, this.player_limit, this.room_id, message);            
        });

        // All players are here: Intro and Survey Trigger
        this.socket.on('start_intro_and_survey', (message) => {
            this.testbed.intro_ready_callback();
        });

        // start game
        this.socket.on("start_game", (message) => {
            
            console.log(this.player_id)
            this.testbed.start_game_callback();


            this.game = new Phaser.Game(this.testbed.phaser_config); // Instantiate the game
            var scene_config = {
                                    "testbed" : this.testbed,
                                    "game" : this.game,
                                    "room_id" : this.room_id,
                                    "player_id" : this.player_id,
                                    "player_limit" : this.player_limit,
                                    "socket" : this.socket
                                };
            this.game.scene.add("ASISTScene", new ASISTScene(scene_config));
            this.game.scene.start("ASISTScene");
        });

        // in game
        this.socket.on('player_update_requested', (message) => {
            if (message['idx'] != this.player_id) {

                // check if there is not a pending request
                if (!document.getElementById("submit-button-div").style.display | document.getElementById("submit-button-div").style.display == "none") {
                    //pre fill with ground truth
                    document.getElementById('player_medkit').value = document.getElementById('player-med-kits-used').textContent;
                    document.getElementById('player_green_vic').value = document.getElementById('green_victim_saved').textContent;
                    document.getElementById('player_yellow_vic').value = document.getElementById('yellow_victim_saved').textContent;
                    document.getElementById('player_score').value = document.getElementById('score').textContent;

                    document.getElementById("request-label").classList.toggle("blink-bg");
                    set_display("submit-button-div", "block");
                    set_display("cancel-button-div", "block");
                }

            }

        });

        this.socket.on('teammate_update_received', (message) => {
            if (message['idx'] != this.player_id) {
                document.getElementById("teammate-time").textContent = message['asist_timer'];
                document.getElementById("teammate-medkit").textContent = message['request_input_form']['player_medkit'];
                document.getElementById("teammate-green-vic").textContent = message['request_input_form']['player_green_vic'];
                document.getElementById("teammate-yellow-vic").textContent = message['request_input_form']['player_yellow_vic'];
                document.getElementById("teammate-score").textContent = message['request_input_form']['player_score'];

                document.getElementById("update-label").classList.toggle("blink-bg");

                // blink for 10 seconds when update recieved
                setTimeout(
                    function() {
                        document.getElementById("update-label").classList.toggle("blink-bg");
                    }, 10000);
            }

        });

        this.socket.on('teammate_msg_received', (message) => {
            if (message['idx'] != this.player_id) {
                document.getElementById("teammate-msg-time").textContent = message['asist_timer'];
                document.getElementById("teammate-msg-received").textContent = message['msg_note'];
                document.getElementById("msg-label").classList.toggle("blink-bg");
                // blink for 5 seconds when note recieved
                setTimeout(
                    function() {
                        document.getElementById("msg-label").classList.toggle("blink-bg");
                    }, 4000);
            }
        });

        this.socket.on('participant_left', (message) => {
            alert('Participant has left room. Refresh to start over or the window will refresh in 4 seconds');

            setTimeout(function() {
                window.location.reload();
            }, 4000);

        });

    }

    start_wait() {
        var prolific = document.getElementById('prolific').value;
        this.prolific = prolific;
        this.socket.emit("start_wait", {
            topic: "event",
            key: "consent",
            publisher: "client",
            idx: this.player_id,
            config: this.testbed.user_config,
            prolificID : prolific,
        });

    }

    participant_ready() {
        this.socket.emit("participant_ready", {
            topic: "event",
            key: "pl_ready",
            publisher: "client",
            idx: this.player_id,
            rm_id: this.room_id,
        });

    }

    msg_send() {
        var player_msg = document.getElementById("player_msg").value;
        var asist_timer = document.getElementById("timer-text").innerHTML;
        document.getElementById("player_msg").value = '';
        console.log(" send");
        document.getElementById("player_msg").blur();
        document.getElementById("player_msg").placeholder = "Message Delivered!"
        document.getElementById("msg_submit").innerHTML = "Sent!"
        document.getElementById("msg_submit").classList.add("button");
        this.socket.emit("teammate_msg_submission", {
            topic: "comm",
            key: "note_msg_submit",
            publisher: "client",
            idx: this.player_id,
            asist_timer: asist_timer,
            msg_note: player_msg,
        });
        setTimeout(function() {
            document.getElementById("player_msg").placeholder = "New Message"
            document.getElementById("msg_submit").innerHTML = "Send"
            document.getElementById("msg_submit").classList.remove("button");


        }, 2500);

    }
    
    survey_completed(key, survey_data) {
        this.socket.emit('end_game_survey', {
            topic: "survey",
            key: key,
            publisher: "client",
            idx: this.player_id,
            survey_results: survey_data,
            prolificID : this.prolific,
            rm_id: this.room_id,
        });

    }

};