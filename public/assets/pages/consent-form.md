Welcome to the Cornell Lightweight Testbed. This study is being led by JiHyun Jeong, Information Science at Cornell University. The Faculty Advisor for this study is Prof. Guy Hoffman, Mechanical and Aerospace Engineering at Cornell University.

**What the study is about:** The purpose of this study is to understand how you work on an online task with mixed incentives.

**What we will ask you to do:** If you agree to be in this study, we will ask you to work on a search and rescue task. First you will read our instructions on how to play and complete the tasks in the game. You will be quizzed on the contents as an attention check so please read carefully. You will then proceed to the actual game once you and your teammate both finish the quiz. You will have 6 minutes to play the game with your teammate. Afterwards, you will be asked to complete a survey questionnaire about your experience. The study should take up to 15 minutes to complete.

**Risks:** We do not anticipate any risks to you participating in this study other than those encountered in day-to-day life. We anticipate that your participation in surveys presents no greater risk than everyday use of the Internet.

**Benefits:** There are no direct benefits to you.

**Privacy / Confidentiality / Data Security** We do not collect identifiable data other than your Prolific ID; only the researchers will have access to records that link your Prolific ID to your study data. Researchers cannot access any participants’ identifiable information through their prolific IDs. We will not include any information that will make it possible to identify you in any sort of public report.

**Data Sharing** De-identified data from this study may be shared with the research community at large to advance science and health. We will remove or code any personal information that could identify you before files are shared with other researchers to ensure that, by current scientific standards and known methods, no one will be able to identify you from the information we share. Despite these measures, we cannot guarantee anonymity of your personal data.

**Taking part is voluntary** Taking part in this study is completely voluntary. If you are affiliated with Cornell University and decide not to take part in this study, it will not affect your current or future relationship with Cornell University. If you decide to take part, you are free to withdraw at any time.

**If you have questions** The researchers conducting this study are JiHyun Jeong, Stanley Celestin, and Prof. Guy Hoffman. Please ask any questions you have now. If you have questions later, you may contact JiHyun Jeong at [jj639@cornell.edu](mailto://jj639@cornell.edu).You can reach Stanley Celestin at [sc3246@cornell.edu](mailto://sc3246@cornell.edu). You can reach Prof. Hoffman at [hoffman@cornell.edu](mailto://hoffman@cornell.edu). If you have any questions or concerns regarding your rights as a subject in this study, you may contact the Institutional Review Board (IRB) at 607-255-5138 or access their website at http://www.irb.cornell.edu. You may also report your concerns or complaints anonymously through Ethicspoint (www.hotline.cornell.edu) or by calling toll free at 1-866-293-3077. Ethicspoint is an independent organization that serves as a liaison between the University and the person bringing the complaint so that anonymity can be ensured.

**Statement of Consent:** I have read the above information, and have received answers to any questions I asked. I consent to take part in the study.
