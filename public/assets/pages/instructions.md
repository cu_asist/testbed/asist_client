    

**Welcome!!** to our team search-and-rescue game! 
<br><br>


*Please read the game instructions carefully. You will be quizzed on the content before you can start the game*

*<u>Please be careful **not to refresh the page at any point**. You and your teammate will have to start over when you refresh</u>*

<br><br>

### What will you be doing?
Your team's mission is to **find and rescue victims from a building**.<br>

You will have **<span id="game_time">6 minutes</span>** to search and rescue victims.

To navigate corridors and rooms in the building, use the **up, down, left, and right arrow keys**. <br>

<table>
<tr>
    <td><b>You</b> are the <span id="player_type">green</span> player <img id ="avatar_image" class="icon" src="assets/images/player_g.png" alt="The Player"></td>
    <td>&nbsp;&nbsp;and <b>your teammate</b> is the <span id="teammate_type">yellow</span> player <img id ="teammate_avatar_image" class="icon" src="assets/images/player_y.png" alt="The Teammate"></td>
</tr>
</table>

<br><br>


### How do you save a victim?
To save a victim, press the **"r" key** when you are next to a victim you'd like to save.<br>

There are **15 green victims <img class="icon" src="assets/images/green_victim.png" alt="Green Victim"> and 15 yellow victims <img class="icon" src="assets/images/yellow_victim.png" alt="Yellow Victim">** in the game.<br>

When saved, the victim will visually change state to "saved".

<br>

<table>
<tr>
    <td>Green Victim</td>
    <td>Yellow Victim</td>
    <td>Saved Victim</td>
</tr>
<tr>
    <td><img class="icon" src="assets/images/green_victim.png" /></td>
    <td><img class="icon" src="assets/images/yellow_victim.png" /></td>
    <td><img class="icon" src="assets/images/green_victim_saved.png" /></td>    
</tr>
</table>
<br>

<br><br>


### Rewards for saving a victim!

You will be earning **extra financial rewards when you save a victim!**<br>
However, you can **only save a victim if the team has enough shared resources** called medical kits<span><img id ="medical_kit_img" class="icon" src="assets/images/medkit.png" alt="The medical kit"></span> for the victim.

**30 medical kits are given to the whole team.**<br> 
Every time you or your teammate save any victim, the number of shared medical kits will decrease.<br>

The color of the victim determines how many medical kits are needed and the reward for saving the victim.

If a player saves a victim of **their own color**, only the **rescuing player will earn $.3**,<br> and **2 medical kits** will be used from the shared pool of medical kits.<br>

If a player saves a victim of **their teammate's color**, **both the player and their teammate would earn $.1 each**,<br> and **1 medical kit** will be used.

<table>
<tr>
    <td>save</td>
    <td><img src="assets/images/green_victim.png" width="30" alt="green victim"></td>
    <td><img src="assets/images/yellow_victim.png" width="30" alt="yellow victim"></td>
</tr>
<br>

<tr>
    <td><br><b>YOU</b>(<img id ="avatar_image2" class="icon" src="assets/images/player_g.png" alt="The Player">)</td>
    <td>You  + $<span id="player_rew_for_green">0.3</span> <br>&nbsp;&nbsp; Teammate  +$<span id="teammate_rew_for_green">0</span>&nbsp;&nbsp;</td>
    <td>You  + $<span id="player_rew_for_yellow">0.1</span> <br> &nbsp;&nbsp;Teammate  +$<span id="teammate_rew_for_yellow">0.1</span>&nbsp;&nbsp;</td>

</tr>
<tr>
    <td></td>
    <td> -<span id="player_med_for_green"> 2</span> med kit</td>
    <td> -<span id="player_med_for_yellow"> 1</span> med kit</td>
</tr>
<tr>
    <td><br></td>
    <td></td>
    <td></td>
</tr>

<tr>
    <td><br><b>TEAMMATE</b>(<img id ="teammate_avatar_image2" class="icon" src="assets/images/player_y.png" alt="The Teammate">)</td>
    <td>&nbsp;&nbsp;You  + $<span id="player_rew_for_tmgreen">0.3&nbsp;&nbsp;</span><br>&nbsp;&nbsp; Teammate  +$<span id="teammate_rew_for_tmgreen">0</span>&nbsp;&nbsp;</td>
    <td>You  + $<span id="player_rew_for_tmyellow">0.1</span><br> &nbsp;&nbsp;Teammate  +$<span id="teammate_rew_for_tmyellow">0.1</span>&nbsp;&nbsp;</td>
</tr>
<tr>
    <td></td>
    <td> -<span id="teammate_med_for_green">1</span> med kit</td>
    <td> -<span id="teammate_med_for_yellow">2</span> med kit</td>
</tr>
</table>
<br><br><br>

Your team has a chance to save **all 30 victims<br> only if both you and your teammate collaborate to save each other's color victims.**<br>
If so, both players will be earning $.1 per all victims saved by the team. <br>
Combined earnings between you and your teammate could be higher if you both collaborate.<br>

On the other hand, if you or your teammate save victims of your own color,<br> they will earn a higher reward of $.3 per victim.<br>
The team, however, will run out of medical kits before saving all 30 victims. <br>
To earn more money, you will have to **compete with your teammate to save more victims before medical kits run out**.

<br><br>

### Communicate with your teammate!
You and your teammate can send short messages to each other via a text field located on the left side panel.<br>
Below is an example game screen that you can expect to see.
<br><br>

<img id="example_game_screen" alt="Example Game Screen" width="600" />
 


**Good luck!** 
<img id="good_luck_player" class="icon" src="assets/images/green_player.png" alt="Player avatar image"/>

