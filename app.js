var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var no_ssl = process.env.NO_SSL || false;


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res, next) {
    res.render('testbed');
});

app.get('/js/socketaddr.js', function(req, res, next) {

    res.set({ 'Content-Type': 'text/javascript' });
//    res.send('export var socketURL =  "http://localhost:8084";');
//    return;

    os = require('os')
    if (no_ssl) {
        dns = require('dns')
        dns.lookup(os.hostname(), function(err, addr, fam) {
            res.send('export var socketURL =  "http://' + addr + ':8084";');
        });
    } else {
        common_name = process.env.SSL_CN;
        res.send('export var socketURL =  "https://' + common_name + ':8084";');
    }
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
