# Running the Testbed Locally 
Once you have installed and run the testbed locally using these [instructions](./local_installation.md) you will only need to do the following each time you want to run the testbed:
1. In one terminal, run  `$ redis-server`
2. In a different terminal, navigate to the testbed directory, and in that terminal: ` $ cd asist_api`
3. Activate the virtual environment: `$ source asist-venv/bin/activate` (**asist-venv\Scripts\activate.bat** on Windows)
4. Start the API server **asist_api** by running `$ python main.py` 
5. The server should be should be running at **http://localhost:8084**
6. In a third terminal, , navigate to the testbed directory, and in that terminal: `$ cd asist_client`
7. Start node server using  `$ npm run serverstart`  (debug mode) or  `$ npm run devstart` (development mode with automatic restart on file changes) or `$ npm run start`
8. The client should be should be available at **http://localhost:8083**
9. To run the client in unencrypted mode (see: above) without changing the default / environment variable, you can run (e.g.,)  `$ NO_SSL=True npm run serverstart `
10. To run the client on a different port (e.g., 8888) you can run (e.g.,)  `$ PORT=8888 npm run serverstart `

Below is a shell script to do all of the steps listed above. It should run in your testbed directory.

```
#!/bin/bash

killall redis-server

pid=$(lsof -i:8083 -t); kill -TERM $pid || kill -KILL $pid
pid=$(lsof -i:8084 -t); kill -TERM $pid || kill -KILL $pid

sleep 1

source asist_env/bin/activate

(trap 'kill 0' SIGINT; redis-server & python asist_api/main.py & cs asist_client; npm run serverstart )

echo === Cornell ASIST Testbed Running ===

```
