# Local Installation

## Prerequisites
- Redis 6.0.8+ installed (Download [here](https://redis.io/download))
    - Test Redis by running `$ redis-server`
    - Kill redis-server by running `$ killall redis-server`
- Python 3.6.4+ installed (Download [here](https://www.python.org/downloads/))
- NodeJS installed (Download [here](https://nodejs.org/en/download/))

## Clone Repository 
1. Navigate to the directory you want to have the lightweight testbed on your local machine. Create a folder and navigate to it.
    - Example: ` $ mkdir asist; cd asist`
2. Clone both repositories, which can be found [here](https://gitlab.com/cu_asist/testbed). Make sure you get the main branch.
    - Clone the API repo: `$ git clone https://gitlab.com/cu_asist/testbed/asist_api.git`
    - Clone the Client repo: `$ git clone https://gitlab.com/cu_asist/testbed/asist_client.git`
3. Ensure that you are on the **main** branch. 

## Set Up Firebase
You need to have a Firebase account / project / database that the testbed will write to.

### Account, Project, & Database 
1. Create account if not using existing account: https://firebase.google.com/
2. Create project by clicking “Add Project” in the console
3. Once your project exists, open it and create a Realtime Database from the left-side menu. 

### CLI Tools
The Firebase Admin Python SDK is available via pip. You can install the library for all users via sudo:  `$ sudo pip install firebase-admin`. Or, you can install the library for just the current user by passing the --user flag: ` $ pip install --user firebase-admin`

### Authentication Credentials
You need a private key to access Firebase from your PC. Generate a private key file for your service account using the following steps: 
1. In the Firebase console, open **Settings (the gear icon next to “Project Overview”) > Users and permissions > Service Accounts**. 
2. Click Generate New Private Key, then confirm by clicking Generate Key. 
3. Securely store the JSON file containing the key. [Adapted from Firebase-Admin]

## Set up Environment Variables
You always need the following two environment variables:
1. **GOOGLE_APPLICATION_CREDENTIALS** is the path to the json private key file you downloaded above.
2. **FIREBASE_URL** is the URL of the Realtime Database on Firebase which can be found under Realtime Database in the left-side menu. It is the URL at the top of the database view.

In your .bash_profile file, add the following lines using the information above:
1. `export GOOGLE_APPLICATION_CREDENTIALS="/path/to/your/credentials.json"`
2. `export FIREBASE_URL="https://your-fb-database-url.firebaseio.com"`

### Running Unencrypted Using HTTP/WS
The testbed runs by default on HTTPS/WSS. If you want to run without encryption (HTTP/WS), you need to set the environment variable NO_SSL to True.

In your .bash_profile file, add the following line:

`export NO_SSL=True`

### Running Encrypted Using HTTPS/WSS (Default)
The testbed runs by default on HTTPS/WSS. If you don’t change this (see: above), you need to have an SSL certificate for your server. Contact your network administrator if you need help with that. This [tutorial](https://itnext.io/node-express-letsencrypt-generate-a-free-ssl-certificate-and-run-an-https-server-in-5-minutes-a730fbe528ca) might also be helpful.

Then, you also need to set three additional variables:

1. **SSL_KEY** is the path to the SSL private key file for your server.
2. **SSL_CERT** is the path to the SSL certificate for your server.
3. **SSL_CN** is the “Common Name” on your certificate (usually the domain name of the server).

In your .bash_profile file, add the following lines using the information above:

1. `export SSL_KEY="/path/to/your/ssl-key"`
2. `export SSL_CERT="/path/to/your/ssl-certificate"`
3. `export SSL_CN="my.domain.name.edu"`

You might have to use different script commands if you are using a different shell (not bash). 
Reload your environment by starting a new terminal window. 


## Set up a Virtual python environment and install API dependencies 

In your project directory, run the following commands:
1. `$ python3 -m venv --upgrade-deps asist-venv`
2. ` $ source asist-venv/bin/activate ` (asist-venv\Scripts\activate.bat on Windows)
3. `$ cd asist_api`

Update your environment by running 
1. `$ pip install --upgrade pip `
2. `$ pip install --upgrade setuptools`
3. `$ pip install -r requirements.txt`

## Run API Server
1. In a new terminal, run `$ redis-server`
2. In the terminal that is running the virtual environment,  start the API server **asist_api** by running `$ python main.py `
3. The server should be should be running at **http://localhost:8084**
4. To run the server in unencrypted mode (see: above) without changing the default / environment variable, you can run `$ NO_SSL=True python3 main.py `

## Setup Client
1. In a new terminal, navigate to the asist_client directory
2. Install node modules using `$ npm install ` from the current directory of the client repository. No need to be in the python environment you created above.
3. Start node server using  `$ npm run serverstart`  (debug mode) or  `$ npm run devstart` (development mode with automatic restart on file changes) or `$ npm run start`
4. The client should be should be available at http://localhost:8083 
5. To run the client in unencrypted mode (see: above) without changing the default / environment variable, you can run (e.g.,)  `$ NO_SSL=True npm run serverstart `
6. To run the client on a different port (e.g., 8888) you can run (e.g.,)  `$ PORT=8888 npm run serverstart `


